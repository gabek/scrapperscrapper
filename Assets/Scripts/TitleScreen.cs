﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleScreen : MonoBehaviour
{
    public AudioSource StartSFX;
    public AudioSource TitleTheme;
    public AudioSource ConfirmSFX;
    public Text Space;
    public Image Black;
    public GameObject StartButton;
    public GameObject CreditButton;
    public GameObject QuitButton;
    public GameObject BackButton;
    public GameObject CreditScreen;
   

    
    private float flashTime;
    private float startTime;
    private float startWait;
    private bool start;
    private bool menu;
    private bool credits;


    void Start()
    {
        CreditScreen.SetActive(false);
        Space.enabled = false;
        StartButton.SetActive(false);
        StartButton.GetComponent<Button>().onClick.AddListener(StartGame);
        CreditButton.SetActive(false);
        CreditButton.GetComponent<Button>().onClick.AddListener(OpenCredits);
        QuitButton.SetActive(false);
        QuitButton.GetComponent<Button>().onClick.AddListener(QuitGame);
        BackButton.SetActive(false);
        BackButton.GetComponent<Button>().onClick.AddListener(CloseCredits);

        flashTime = Time.time;
        startTime = 0f;
        startWait = 4f;
        start = false;
        menu = false;
        credits = false;
    }

    void FixedUpdate()
    {
        if (Time.time > flashTime && !menu)
        {
            FlashStart();
        }
        if (Input.GetKey(KeyCode.Space) && !menu)
            OpenMenu();
        if (start)
        {
            FadeToBlack();
            if (Time.time > startTime)
                SceneManager.LoadScene("Beta Map");
        }
    }

    private void OpenMenu()
    {
        menu = true;
        Space.enabled = false;
        ConfirmSFX.Play();
        StartButton.SetActive(true);
        QuitButton.SetActive(true);
        CreditButton.SetActive(true);
    }

    private void StartGame()
    {
        StartButton.SetActive(false);
        QuitButton.SetActive(false);
        CreditButton.SetActive(false);
        TitleTheme.Stop();
        StartSFX.Play();
        GeneratePreferences();
        start = true;
        startTime = Time.time + startWait;
    }

    

    private void OpenCredits()
    {
        StartButton.SetActive(false);
        QuitButton.SetActive(false);
        CreditButton.SetActive(false);
        BackButton.SetActive(true);
        ConfirmSFX.Play();
        CreditScreen.SetActive(true);
        credits = true;
    }

    private void CloseCredits()
    {
        StartButton.SetActive(true);
        QuitButton.SetActive(true);
        CreditButton.SetActive(true);
        BackButton.SetActive(false);
        ConfirmSFX.Play();
        CreditScreen.SetActive(false);
        credits = false;
    }

    private void QuitGame()
    {
        ConfirmSFX.Play();
        Application.Quit();
    }

    private void FadeToBlack()
    {
        float alphaPercent = (startWait - (startTime - Time.time))/ startWait;
        Black.color = new Color(0, 0, 0, alphaPercent);
    }

    private void FlashStart()
    {
        if (Space.enabled)
            Space.enabled = false;
        else
            Space.enabled = true;
        flashTime = flashTime + 1f;
    }

    private void GeneratePreferences()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetFloat("XLocation", -.5f);
        PlayerPrefs.SetFloat("YLocation", -0.5f);
        PlayerPrefs.SetInt("PlayerHealth", 16);
        PlayerPrefs.SetInt("Repairs", 3);
    }
}
