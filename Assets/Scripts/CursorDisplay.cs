﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorDisplay : MonoBehaviour
{
    public GameObject Cursor;

    void Start()
    {
        Cursor.SetActive(false);
    }

    public void TurnOff()
    {
        Cursor.SetActive(false);
    }
}
