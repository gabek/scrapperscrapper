﻿using System;
using System.Collections;
using UnityEngine;
using Random = System.Random;

class EnemyBattler : Battler
{
    public AudioSource HitSfx;
    private const int moveTime = 13;
    private const float xmove = 4f;
    private const float ymove = 4f;


    public override (string, int) Attack()
    {
        Random random = new Random();

        int damage = random.Next(1, this.AttkDice) + this.StrMod;
        StartCoroutine(AttackAnim());
        HitSfx.Play();
        return ("Player", damage);
    }

    public override (string, int) Item()
    {
        return ("Enemy", 0);
    }

    private IEnumerator AttackAnim()
    {
        for (int i = 0; i < moveTime; i++)
        {
            transform.Translate(-1 * xmove * Time.deltaTime, -1 * ymove * Time.deltaTime, 0);
            yield return new WaitForSeconds(.01f);
        }
        yield return new WaitForSeconds(.1f);
        for (int i = 0; i < moveTime; i++)
        {
            transform.Translate(xmove * Time.deltaTime, ymove * Time.deltaTime, 0);
            yield return new WaitForSeconds(.01f);
        }
    }

}
