﻿using System;
using System.Collections;
using UnityEngine;
using Random = System.Random;


class PlayerBattler : Battler
{
    public int Repairs;
    public AudioSource AttackSfx;
    public AudioSource HealSfx;

    private const float xmove = 4f;
    private const float moveTime = 13;

    public override (string, int) Attack()
    {
        Random random = new Random();
        StartCoroutine(AttackAnim());
        int damage = random.Next(1, this.AttkDice) + this.StrMod;
        AttackSfx.Play();
        return ("Enemy", damage);
    }

    public override (string, int) Item()
    {
        Repairs--;
        HealSfx.Play();
        return ("Player", 10);
    }

    private IEnumerator AttackAnim()
    {
        for (int i = 0; i < moveTime; i++)
        {
            transform.Translate(xmove * Time.deltaTime, 0, 0);
            yield return new WaitForSeconds(.01f);
        }
        yield return new WaitForSeconds(.1f);
        for (int i = 0; i < moveTime; i++)
        {
            transform.Translate(-1*xmove * Time.deltaTime, 0, 0);
            yield return new WaitForSeconds(.01f);
        }
    }
}
