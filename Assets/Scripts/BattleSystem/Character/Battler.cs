﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Battler : MonoBehaviour
{
    public int HP { get; private set; }
    public string Name;
    public int MaxHP;
    public int StrMod;
    public int AttkDice;

    public AudioSource Explosion;

    private const float flashTime = .1f;
    private float deathTime = .5f;

    public void Start()
    {
        HP = MaxHP;
    }

    public void ChangeHP(int change)
    {
        //if (change < 0)
        //     StartCoroutine(FlashSprite());
        HP = HP + change;
        if (HP > MaxHP)
            HP = MaxHP;
        else if (HP < 0)
            HP = 0;
    }

    public IEnumerator<object> FlashSprite()
    {
        for (int i = 0; i < 3; i++)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            yield return new WaitForSeconds(flashTime);
            GetComponent<SpriteRenderer>().enabled = true;
            yield return new WaitForSeconds(flashTime);
        }

    }

    public IEnumerator<object> Death()
    {
        Explosion.Play();
        for (int i = 1; i <= 10; i++)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            yield return new WaitForSeconds(deathTime/i);
            GetComponent<SpriteRenderer>().enabled = true;
            yield return new WaitForSeconds(deathTime/i);
        }
        GetComponent<SpriteRenderer>().enabled = false;
    }

    public void LoadHP(string variable)
    {
        HP = PlayerPrefs.GetInt(variable, MaxHP);
    }

    //TODO: Move attack to here
    // Make Attack(string Target)? or maybe Attack(string[] target?)
    public abstract (string, int) Attack();

    public abstract (string, int) Item();
}

