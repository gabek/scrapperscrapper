﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BattleState
{
    PlayerTurn,
    PlayerSelect,
    EnemyTurn,
    CheckState,
    EndBattle
}
