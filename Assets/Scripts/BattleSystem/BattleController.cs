﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BattleController : MonoBehaviour
{
    private BattleState currentState;
    private TurnOrder turnOrder;
    private int turn;
    private AssetBundle myLoadedAssetBundle;
    private Func<(string, int)> playerAct;
    private Button Attack;
    private Button Repair;

    private bool startBattle;
    private bool battle;
    private bool exitBattle;
    private const float explosionTime = 6f;
    private int scrapped = 0;

    public float turnTime;

    public GameObject Player;
    public GameObject Enemy;
    public Text BattleBox;
    public Text EnemyName;
    public Text HP;
    public GameObject AttackButton;
    public GameObject RepairButton;
    public AudioSource Confirm;
    public AudioSource Cancel;
    public AudioSource Jingle;
    public AudioSource BattleTheme;

    // Start is called before the first frame update
    void Start()
    {
        AttackButton.SetActive(false);
        RepairButton.SetActive(false);
 

        var enemyBattler = Enemy.GetComponent<EnemyBattler>();
        BattleBox.text = enemyBattler.Name + " Appeared!";
        if (enemyBattler != null)
            EnemyName.text = enemyBattler.Name;
        turn = 0;
        turnOrder = new TurnOrder();
        turnTime = 0f;

        var playerBattler = Player.GetComponent<PlayerBattler>();
        playerBattler.LoadHP("PlayerHealth");
        playerBattler.Repairs = PlayerPrefs.GetInt("Repairs",3);
        CreateHPText();
        CreateRepairText();

        currentState = BattleState.PlayerSelect;
        scrapped = PlayerPrefs.GetInt("Scrapped", 0);
        
        Attack = AttackButton.GetComponent<Button>();
        Attack.onClick.AddListener(CallAttack);
        Repair = RepairButton.GetComponent<Button>();
        Repair.onClick.AddListener(CallRepair);
        battle = true;
        exitBattle = false;
    }

    

    private void FixedUpdate()
    {
        if (Time.time > turnTime && Input.GetMouseButton(0) && battle)
        {
            Battle();
        }
        else if(exitBattle && Input.GetMouseButtonDown(0))
        {
            if (scrapped < 3)
                SceneManager.LoadScene(PlayerPrefs.GetString("OverworldMap"));
            else
                SceneManager.LoadScene("Victory");
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            Confirm.Play();
            SceneManager.LoadScene("TitleScreen");
        }
    }

    // Update is called once per frame
    void Battle()
    { 
       
        var playerBattler = Player.GetComponent<PlayerBattler>();
        var enemyBattler = Enemy.GetComponent<EnemyBattler>();
        switch (currentState)
        {
            case BattleState.PlayerSelect:
                if(!AttackButton.activeSelf)
                {
                    BattleBox.text = "Select Action:";
                    AttackButton.SetActive(true);
                    RepairButton.SetActive(true);
                }
                break;
            case BattleState.PlayerTurn:
                (string target, int damage) = playerAct();
                if (target == "Enemy")
                {
                    enemyBattler.ChangeHP(-1 * damage);
                    StartCoroutine(Damage(enemyBattler));
                    BattleBox.text = playerBattler.Name + " attacked " + enemyBattler.Name + " for " + damage + " damage!";
                }
                else if (target == "Player")
                {
                    playerBattler.ChangeHP(damage);
                    CreateHPText();
                    if (playerBattler.Repairs <= 0)
                        RepairButton.GetComponentInChildren<Text>().color = new Color(0.827f, 0.827f, 0.827f);
                    CreateRepairText();
                    BattleBox.text = playerBattler.Name + " repaired itself " + damage + " points of damage!";
                }
                
                UpdateTurnTime();
                currentState = BattleState.CheckState;
                break;

            case BattleState.EnemyTurn:
                (target, damage) = enemyBattler.Attack();
                BattleBox.text = enemyBattler.Name + " attacked " + playerBattler.Name + " for " + damage + " damage!";
                StartCoroutine(Damage(playerBattler));
                playerBattler.ChangeHP(-1 * damage);
                CreateHPText();
                UpdateTurnTime();
                currentState = BattleState.CheckState;
                break;

            default:
                if (enemyBattler.HP <= 0)
                {
                    StartCoroutine(enemyBattler.Death());
                    StartCoroutine(Victory(enemyBattler));
                    battle = false;
                }
                if (playerBattler.HP <= 0)
                {
                    StartCoroutine(playerBattler.Death());
                    StartCoroutine(GameOver(playerBattler));
                    battle = false;
                }
                turn++;
                currentState = turnOrder.GetTurn(turn);
                UpdateTurnTime();
                break;
        }
        
        
    }


    private void CreateHPText()
    {
        var battler = Player.GetComponent<PlayerBattler>();
        if (battler != null)
            HP.text = battler.HP + "/" + battler.MaxHP;
    }

    private void CreateRepairText()
    {
        var playerBattler = Player.GetComponent<PlayerBattler>();
        if(playerBattler != null)
        {
            var remaining = "";
            if(playerBattler.Repairs>0)
                remaining = $" X {playerBattler.Repairs}";
            RepairButton.GetComponentInChildren<Text>().text = $"Repair{remaining}";
        }
    }

    private IEnumerator Victory(Battler battler)
    {
        BattleBox.text = $"{battler.Name} was Scrapped!!!";
        yield return new WaitForSeconds(explosionTime);
        currentState = BattleState.EndBattle;
        BattleTheme.Stop();
        Jingle.Play();
        var playerBattler = Player.GetComponent<PlayerBattler>();
        BattleBox.text = $"{playerBattler.Name} was victorious!!!";
        scrapped++;
        PlayerPrefs.SetInt("Scrapped", scrapped);
        PlayerPrefs.SetInt("PlayerHealth", playerBattler.HP);
        PlayerPrefs.SetInt("Repairs", playerBattler.Repairs);
        yield return new WaitForSeconds(.5f);
        exitBattle = true;
        
    }

    private IEnumerator GameOver(Battler battler)
    {
        BattleBox.text = $"{battler.Name} was Scrapped!!!";
        yield return new WaitForSeconds(explosionTime);
        SceneManager.LoadScene("GameOver");
    }

    private void CallAttack()
    {
        var playerBattler = Player.GetComponent<PlayerBattler>();
        playerAct = playerBattler.Attack;
        BattleBox.text = playerBattler.Name + " is attacking!";
        EndPlayerSelect();
    }

    private void CallRepair()
    {

        var playerBattler = Player.GetComponent<PlayerBattler>();
        if (playerBattler.Repairs > 0)
        {
            playerAct = playerBattler.Item;
            BattleBox.text = playerBattler.Name + " is repairing itself!";
            EndPlayerSelect();
        }
        else
            Cancel.Play();
    }

    private void EndPlayerSelect()
    {
        currentState = BattleState.PlayerTurn;
        AttackButton.SetActive(false);
        RepairButton.SetActive(false);
        AttackButton.GetComponent<CursorDisplay>().TurnOff();
        RepairButton.GetComponent<CursorDisplay>().TurnOff();
        UpdateTurnTime();
        Confirm.Play();
    }

    private IEnumerator Damage(Battler battler)
    {
        yield return new WaitForSeconds(.26f);
        StartCoroutine(battler.FlashSprite());
    }

    private void UpdateTurnTime()
    {
        turnTime = Time.time + .25f;
    }
}