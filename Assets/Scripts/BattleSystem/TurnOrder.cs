﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOrder : MonoBehaviour
{
    public List<BattleState> turns { get; }

    public TurnOrder()
    {
        turns =  new List<BattleState>() { BattleState.PlayerSelect, BattleState.EnemyTurn};
    }

    public BattleState GetTurn(int i)
    {
        int index = i % turns.Count;
        return turns[index];
    }
}
