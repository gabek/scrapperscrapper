﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D Player;
    private Animator Animator;
    private Vector2 previousLocation;

    private bool move;
    private float distance;
    private bool checkDistance;
    private bool encounter;
    private float encounterDistance;

    public GameObject Flash1;
    public GameObject Flash2;
    public AudioSource OverworldTheme;
    public AudioSource Confirm;

    public float speed;
    public float directX;
    public float directY;
    public string map;

    // Start is called before the first frame update
    void Start()
    {
        Player = GetComponent<Rigidbody2D>();
        Player.position = new Vector2(PlayerPrefs.GetFloat("XLocation", -0.5f), PlayerPrefs.GetFloat("YLocation", -0.5f));
        previousLocation = Player.position;
        Animator = GetComponent<Animator>();
        move = false;
        checkDistance = false;
        encounter = false;
        encounterDistance = 10f;
        Animator.SetFloat("DirectX", directX);
        Animator.SetFloat("DirectY", directY);
        Animator.SetBool("Move", move);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Confirm.Play();
            SceneManager.LoadScene("TitleScreen");
        }
        if (!checkDistance)
            StartCoroutine(CheckDistance());
        if (distance >= encounterDistance  && move)
        {

            if (!encounter)
            {             
                Random rand = new Random(DateTime.Now.Millisecond);
                var result = rand.Next(0, 20);
                print(result);
                if (result == 0)
                    EnemyEncounter();
                else
                    encounterDistance = distance + 1;
            }
            
        }
        if(!encounter)
        {
            var startLocation = Player.position;
            var moveX = Input.GetAxis("Horizontal");
            var moveY = Input.GetAxis("Vertical");

            Vector2 movement = new Vector2(moveX, moveY);

            if (!Mathf.Approximately(movement.x, 0.0f) || !Mathf.Approximately(movement.y, 0.0f))
            {
                (directX, directY) = CreateDirection(movement.x, movement.y);

                move = true;
                Animator.SetFloat("DirectX", directX);
                Animator.SetFloat("DirectY", directY);
                Animator.SetBool("Move", move);

            }
            else
            {
                move = false;
                Animator.SetBool("Move", move);
            }
            Player.velocity = speed * movement;
        }
    }

    

    private (float, float) CreateDirection(float moveX, float moveY)
    {
        float directionX;
        float directionY;

        if (moveX > 0)
            directionX = 1;
        else if (moveX < 0)
            directionX = -1;
        else
            directionX = 0;

        if(moveY >0)
            directionY = 1;
        else if (moveY < 0)
            directionY = -1;
        else
            directionY = 0;

        return (directionX, directionY);
    }

    private IEnumerator CheckDistance()
    {
        checkDistance = true;
        yield return new WaitForSeconds(.1f);
        distance += (Vector2.Distance(Player.position, previousLocation));
        previousLocation = Player.position;
        print(distance);
        checkDistance = false;
    }

    private void EnemyEncounter()
    {
        Player.velocity = 0f * new Vector2(0,0);
        Animator.SetBool("Move", false);
        OverworldTheme.Stop();
        StartCoroutine(EncounterAnimation());
        PlayerPrefs.SetFloat("XLocation", Player.position.x);
        PlayerPrefs.SetFloat("YLocation", Player.position.y);
        PlayerPrefs.SetString("OverworldMap", map);
        encounter = true;
    }

    private IEnumerator EncounterAnimation()
    {
        Flash1.SetActive(true);
        for (int i = 0; i < 2; i++)
        {
            Flash2.SetActive(true);
            yield return new WaitForSeconds(.3f);
            Flash2.SetActive(false);
            yield return new WaitForSeconds(.3f);
        }
        Flash2.SetActive(true);
        SceneManager.LoadScene("BattleScene");
    }
}
