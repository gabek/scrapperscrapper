﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameController : MonoBehaviour
{
    public float Wait;

    private float BackTime;

    void Start()
    {
        BackTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > BackTime+Wait)
        {
            if (Input.anyKey)
                SceneManager.LoadScene("TitleScreen");
        }
    }
}
