### Scrapper Scrapper ###

#### About ####

Scrapper Scrapper is a very small RPG I helped create for Global Game Jam 2020 at the Buffalo NY location.

The original version was built in 72 hrs and released as part of the global game jam. You can play the original [here](https://globalgamejam.org/2020/games/scapper-scrapper-0).

The weeks following the Game Jam I updated the game on my own spare time, adding a simple overworld and some random encounters. 

I was the sole coder for this team.

#### Technology Used ####

The game is built in Unity Engine and as a result uses C# as it's language of choice. The game makes use of Asynchronous methods to execute some animations and events. 

It also demonstrates some basics of object oriented programming like Inheritance and Encapsulation.

The game was built in Visual Studios and hosted on a private source control site. 


#### Other Relevant Info ####

As this was a team effort, I had to coordinate with other team members. Without the rest of the team, the project in its scope would not have been possible. 

I had to meet the standards and requests that the team put on me, and complete the task in a very short time frame. This included doing relevant research on the subject, and building the RPG system from scratch. I had to communicate with Team Mates about desires, and what they wanted to have in the games final build.

#### How To Play ####

##### Install #####

- Unzip the file under [ScrapperExcutable](https://bitbucket.org/gabek/scrapperscrapper/src/master/ScrapperExcutable/) on a windows machine.
- Double click on the exe file.

##### Over World #####
- WASD to move.
- Move around until you get a random battle.


##### Battle #####
- Select Attack to attack the enemy.
- Select Repair to heal 10 hp.
- You only have 3 Repair so use them wisely.
- If you win, you will return to the Overworld.
- If you lose, it's game over.

##### Victory and Ending #####

- Press ESC at any time to return to the title screen.
- Defeat 3 Enemies to Win.
- You can exit the game from the Title Screen.